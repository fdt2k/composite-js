import {curry,pipe,compose,map,identity,diverge} from 'core';
import {test} from 'string'
import {trace} from '../debug'
import {reduce,head,indexOf} from 'array'
import {not} from 'bool'
import { prop , as_prop, keys,ensure_object_copy, assign2, enlist} from 'object';

import {isStrictlyEqual,is_type_object} from 'bool'
import {mergeAll} from 'List'

// Object -> Scalar
// {a:b} -> a
// {a:b, c:d} -> a

/**
 * @params {{}} object
 * @returns string
 */
export const key = compose(head,keys)


export const value = obj=> obj[key(obj)]

/**
 * extracts the first key and the first value of an object
 * @param {{}} obj 
 * @returns {[]}
 */
export const keyval = obj => [key(obj),value(obj)];


//export const objectReduce = reduce({});  //<--- never do this unless you want to keep the accumulator .... forever !!

//  String -> a -> Object -> Bool
export const isPropStrictlyEqual = curry((_prop,value,item)=> compose (isStrictlyEqual(value),prop(_prop)) (item))

export const isPropStrictlyNotEqual = curry((prop,value,item)=> compose(not,isPropStrictlyEqual(prop,value))(item) )
// filter an object and returns key that matches

// regex -> string -> Object -> Bool
export const propMatch = curry((re,key) => compose(test(re),prop(key)));


export const makeHasKey =  k => compose( x=> x!==-1 ,indexOf(k),keys)

export const hasKey = curry((k,o)=> makeHasKey(k)(o) )

// Object -> Object -> Object
export const matchReducer = match => (acc,item)=> {
  //  console.log(head(keys(item)))

    if(match(key(item))){
        return assign2(acc,item)
    }
    return acc;
}
//



export const keepMatching = match => reduce({},matchReducer(match))

export const filterByKey = match => compose(keepMatching(match),trace('x'), enlist,ensure_object_copy)


/*
  perform a match function on every item of an object and returns an array like this:
  [matching, notmatching]

  //MatchFunction => Object => List
*/
export const makeSpreadFilterByKey =transformMatching=> transformNotMatching=> (match)=> compose(
  diverge(
    transformMatching(match),
    transformNotMatching(compose(not,match)),
  ),
  enlist,ensure_object_copy)


/*
  perform a match function on every item of an object and returns an array like this:
  [matching, notmatching]

  //MatchFunction => Object => List
*/
export const spreadFilterByKey = makeSpreadFilterByKey(keepMatching)(keepMatching)



export const spec = curry((obj,arg)=> pipe(
  keys,
  map(x=>as_prop(x,obj[x](arg))),
  mergeAll
)(obj))



/* return a flat object representation of a nested object */
export const pathes = (object,parent_path='',sep='.') => {

  if(keys(object).length ===0){
    return {[parent_path]:{}};
  }

  return enlist(object).reduce((carry,item)=>{
    const [_key,_value] = keyval(item);
    let path = (parent_path=='') ? _key: parent_path+sep+_key;
    if(is_type_object(_value)){
      let subkeys = pathes (_value,path);
      carry = {
        ...carry,
        ...subkeys
      }
   
    }else {
      carry[path] = _value;
    }
    return carry; 
  },{});
}
